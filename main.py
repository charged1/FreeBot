# Imports
import discord
import os
import yaml

from dotenv import load_dotenv
from discord.ext import commands

# Open yaml configs
with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

# Define client
client = commands.Bot(command_prefix=["!"])

# Yaml Var Assign
guild_ids = int(config["GUILDIDS"])

# Load token from .env file
load_dotenv()

# Connect to cogs
for f in os.listdir("./cogs"):
    if f.endswith(".py"):
        client.load_extension("cogs." + f[:-3])

# Startup
@client.event
async def on_ready():
    await client.change_presence(status=discord.Status.online,
    activity=discord.Activity(
      type=discord.ActivityType.watching,
      name="FreeTech Studios"))
    print(f'Logged in as {client.user}!')

# Suggestion command, not sure why it wouldn't work in cogs, but here it is for now.
@client.slash_command(guild_ids=[guild_ids])
async def suggest(ctx, message):
  """Add a suggestion for the server!"""
  if not message:
    await ctx.respond('Please enter a valid suggestion.')
    return

  embed = discord.Embed(
    title='Suggestion',
    description=message,
    color=0xe74c3c
  )

  embed.set_footer(text=f'Requested by {ctx.author}')

  channel = client.get_channel(890955480497786880)
  await channel.send(embed=embed)
  await ctx.respond('Success! Your suggestion was submitted.')

# Get token from .env file
TOKEN = os.environ.get("BOT_TOKEN")

# Run the bot with the token
client.run(TOKEN)
