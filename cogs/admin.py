import discord
import asyncio
import yaml

from discord.ext import commands
from discord.commands import permissions, Option
from discord.ui import Button, View

with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

guild_ids = int(config["GUILDIDS"])

class Admin(commands.Cog, name="Admin"):
  def __init__(self, client):
      self.client = client

  @commands.command()
  @commands.has_permissions(administrator=True)
  async def say(self, ctx, *, message):
    """Sends a message."""
    if not message:
        await ctx.send("Please specify a message to send.")
        return
    await ctx.message.delete()
    await ctx.send(message)

  @commands.command()
  @commands.has_permissions(administrator=True)
  async def staffannounce(self, ctx, *, message):
    """Sends an announcement for staff."""
    if not message:
        await ctx.send("Please specify the announcement message.")
        return
    await ctx.message.delete()
    embed=discord.Embed(title="Attention, Staff!", description=message, color=0xe74c3c)
    await ctx.send(embed=embed)
    await ctx.send('<@&894232208934576198>')

  @commands.command(aliases=['cl'])
  @commands.has_permissions(administrator=True)
  async def changelog(self, ctx, *, message):
    """Make a changelog announcement."""
    if not message:
        await ctx.send("Please enter a change.")
        return
    await ctx.message.delete()
    embed=discord.Embed(title="Changes Made:", description=message, color=0xe74c3c)
    embed.set_footer(text='Check time posted for change time.')
    await ctx.send(embed=embed)
    

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role('👑 Owner')
  async def rawdm(self, ctx, user: discord.User, *, message=None):
    """Sends a raw DM to a user."""

    embed = discord.Embed(
      title='Confirmation',
      description=f'You would like to DM {user.mention}. \nMessage: {message}',
      color=0xe74c3c
    )

    message = message

    buttonyes = Button(label='Yes', style=discord.ButtonStyle.green)
    buttonno = Button(label='No', style=discord.ButtonStyle.red)
      
    async def buttonyes_callback(interaction):
      await user.send(message)
      await interaction.response.edit_message(content=f'DM sent to {user.mention}.', embed=None, view=None)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action cancelled.', embed=None, view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(embed=embed, view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role('👑 Owner')
  async def dm(self, ctx, user: discord.User, *, message=None):
    """Sends an embedded DM to a user."""

    embed = discord.Embed(
      title='Confirmation',
      description=f'You would like to DM {user.mention} in an embed. \nMessage: {message}',
      color=0xe74c3c
    )

    message = message

    dmembed = discord.Embed(
      description=message,
      color=0xe74c3c
    )

    buttonyes = Button(label='Yes', style=discord.ButtonStyle.green)
    buttonno = Button(label='No', style=discord.ButtonStyle.red)
      
    async def buttonyes_callback(interaction):
      await user.send(embed=dmembed)
      await interaction.response.edit_message(content=f'DM sent to {user.mention}.', embed=None, view=None)

    async def buttonno_callback(interaction):
      await interaction.response.edit_message(content='Action cancelled.', embed=None, view=None)

    view = View(buttonyes, buttonno)

    buttonyes.callback = buttonyes_callback
    buttonno.callback = buttonno_callback

    await ctx.respond(embed=embed, view=view)

  @commands.slash_command(guild_ids=[guild_ids])
  @permissions.has_role('👑 Owner')
  async def embed(
      self, ctx,
      title: Option(str, "Enter the title for the embed."),
      description: Option(str, "Please specify the description.",),
      footer: Option(str, "Enter a footer."),
      authorname: Option(str, "Enter the author name."),
  ):
    """Embed maker!"""
  
    embed = discord.Embed(
      title=title,
      description=description,
      color=0xe74c3c
    )

    embed.set_footer(text=footer)

    embed.set_author(name=authorname, icon_url='https://media.discordapp.net/attachments/909150805301530656/919385330044502036/ftsnewlogo.jpg?width=655&height=655')
    
    await ctx.respond('Embed has been generated. You may delete this message.')
    await ctx.send(embed=embed)

def setup(client):
  client.add_cog(Admin(client))