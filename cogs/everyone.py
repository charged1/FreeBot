import discord
import yaml

from discord.ext import commands
from discord.ui import Button, View

with open('config.yaml', 'r') as config_file:
    config = yaml.load(config_file, Loader=yaml.BaseLoader)

guild_ids = int(config["GUILDIDS"])

class Everyone(commands.Cog, name="Everyone"):
  def __init__(self, client):
      self.client = client

  @commands.slash_command(guild_ids=[guild_ids])
  async def ping(self, ctx):
    """Replies with the bot latency."""
    await ctx.respond(f"**:ping_pong: Pong!** Latency: {round(self.client.latency * 1000)}ms")

  @commands.slash_command(guild_ids=[guild_ids])
  async def help(self, ctx):
    """Replies with a link to the help page."""
    
    embed = discord.Embed(
      title='FreeBot Commands',
      description='https://bit.ly/ftscmds',
      color=0xe74c3c
    )

    embed.set_footer(text='Note: The doc does not work very well on mobile.')

    button = Button(label='Help', style=discord.ButtonStyle.url, url='https://bit.ly/ftscmds')
      
    view = View(button)

    await ctx.respond(embed=embed, view=view)

def setup(client):
  client.add_cog(Everyone(client))